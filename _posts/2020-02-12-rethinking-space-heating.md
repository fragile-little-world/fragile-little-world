---
topic: Green Alternatives
date: 2020-02-12
layout: post
title: "Rethinking Space Heating"
---

<picture>
  <source srcset="/assets/images/rethinking-space-heating/EnergyForSpaceHeating.webp" type="image/webp">
  <source srcset="/assets/images/rethinking-space-heating/EnergyForSpaceHeating.jpg" type="image/jpeg">
  <img src="assets/images/rethinking-spaces-heating/EnergyForSpaceHeating.jpg" alt="EIA Chart Showing Energy Usage In Various Home Types">
</picture>_Energy usage in various home types, broken down by use. Notice that heating is the largest single contributor for all but high density apartments.[^1]_

A common strategy for reducing the energy consumption for heating is to set a central thermostat to a lower temperature and heat only the spaces that are currently in use. This can be effective, but careful use of alternatives can reduce energy usage further.

<!--more-->

## Does it make a difference to turn down your thermostat?

The short answer is yes. As seen in the EIA graphic above, home heating is either the most energy intensive or second most energy intensive activity in a home in the United States. Though other areas may have more potential to reduce energy usage when considering the full lifecycle of the items used in the home, home heating is the greatest primary energy demand. This is especially important for carbon emissions, as home heating systems often use fossil fuels like natural gas, propane, or oil. Even for electric heating, common electric resistance heating is an inefficient use of electricity that is mostly generated from fossil fuels.

Turning down the thermostat has a direct relationship to the amount of energy needed to heat the home. The conduction of heat from the home to the surroundings is given by Fourier's Law, described by the equation below[^2].

<picture>
  <source srcset="/assets/images/rethinking-space-heating/Equations.webp" type="image/webp">
  <source srcset="/assets/images/rethinking-space-heating/Equations.png" type="image/png">
  <img src="/assets/images/rethinking-space-heating/Equations.png" alt="Fourier's Law">
</picture>

The variables here, in SI units are:
- q, the rate of heat transferred per area in W/m<sup>2</sup>
- k, the material's thermal conductivity per thickness and per Kelvin of temperature difference in W/m&middot;K
- &nabla;T, the temperature difference over the thickness of the material in K/m

The thermal conductivity and thickness of the material are often encapsulated in what is called the R-value in the building industry, also known as the RSI-value in SI units[^3]. Insulation is a key variable to modify, but improvements to insulation are necessarily a longer term step. The thermostat setting governs the temperature inside the home, which directly effects the temperature difference between inside and outside the home. In my research, I was not able to find any concrete analysis on the savings for turning down the thermostat in the winter (or turning it up in the summer!), but I found many estimates of a 2&deg; F change resulting in a cost savings for heating or cooling on the order of 5-10%. To verify this, I pulled two weather data sets[^4] for the city of Chicago, as I know it can have cold winters. One data set represents an average of hourly data from 1981 through 2010, while the other data set represents the last ten years, ending on December 31st, 2019. I analyzed these data sets using a small Python script[^5], which takes the hourly data points, evaluates the temperature difference from two reference indoor temperatures, 68&deg; F (20&deg; C) and 66&deg; F (18.89&deg; C). These differences are then integrated using a trapezoidal approximation to find the total temperature difference in degree-hours. This process yields a reduction in degree hours of 7-8%, as seen in the bar plot below, validating the assertions discussed earlier. For further analysis, I will assume a 5% savings to be conservative as there are many factors at play.

<picture>
  <source srcset="/assets/images/rethinking-space-heating/SavingsPlot.svg" type="image/svg+xml">
  <source srcset="/assets/images/rethinking-space-heating/SavingsPlot.webp" type="image/webp">
  <source srcset="/assets/images/rethinking-space-heating/SavingsPlot.png" type="image/png">
  <img src="/assets/images/rethinking-space-heating/SavingsPlot.png" alt="Plot of Thermostat Temperature Delta Reduction">
</picture>_Plot showing percentage reduction in degree-hours of temperature delta using a lower room temperature. The difference between the data sets may be due to the greater variation in the data set that was not a historical average._

To estimate the energy savings, we start with the total energy consumption of the average Midwestern home, 27548.68 kWh[^1] in 2015. The average fraction for space heating is 43%, so this accounts for 11845.93 kWh. A savings of 5% on this through the heating season by reducing the thermostat set point would reduce energy consumption by 592.30 kWh. In Chicago, the heating season is legally defined as from September 15th through June 1st[^6]. Even if it were cold enough to require heating on every one of those days, which it is usually not, you could run a 1.5 kW forced air space heater for an hour and a half each day and stay within the amount of energy saved by reducing the thermostat.

## Improving Comfort

The 1.5 kW space heater mentioned is the traditional way to improve comfort in a colder space. From personal experience, I know them to be effective, and often only need to operate at something approximating a 50% duty cycle. Given that many people are at work or school during the day, and sleeping through a significant portion of the night, a space heater of this type could be suitable in the situation outlined above. The savings could be quite small, depending on the exact home and the people in it. However, any electrical heating likely uses grid energy, which represents a conversion efficiency of only 30-40% where it is from fossil fuels[^7]. Therefore, the electric heat option must use significantly less than the amount of energy consumed by the furnace (unless the furnace is electric!). This is because the furnace can use the heat of combustion directly and will have an efficiency of at least 50%, with newer furnaces sometimes reaching over 90%[^8].

The forced air space heater does exactly what it sounds like, it heats the space, much like a furnace would. The temperature of the space is one factor in how personal comfort, but there are other ways to help people feel more comfortable in a cooler space. One option is to use a radiant space heater. Though this type of heater will not use less power than a forced air space heater, its heat will not be put into air which will then circulate around the room, away from the occupants. A radiant heater, carefully placed, can provide heat to room occupants and the surfaces and objects they contact most, providing comfort at a lower energy usage. Another option is an electric heat pad or blanket. These can be used in bed or elsewhere in the home, and provide heat the to the people they are warming and nearby surfaces via conduction, one of the most efficient forms of heat transfer. Given the improvement to heat transfer in this way, some of the higher power ratings for electric blankets are at 400 W[^9]. Even with the lower maximum power, it is not always necessary to run heated blankets at full power, further reducing energy usage.

An even simpler option is to use heating pads that are heated on the stove top or in the microwave. These types of heat pads, and the hot water bottles and coal filled containers before them, have been used for hundreds of years to warm beds and people in poorly heated spaces[^10]. I have heat pads of this type in my own household, used both for staying warm and for heat therapy. These heat pads are useful because they are simple and inexpensive. It is possible to make them at home using a sock and a bag of rice, though commercial ones often use gel or wax. It is these heat pads that I will use to do a loose comparison against forced air space heaters, since those are the heating alternatives with which I have the most personal experience to inform an analysis.

<picture>
  <source srcset="/assets/images/rethinking-space-heating/Heatpads.webp" type="image/webp">
  <source srcset="/assets/images/rethinking-space-heating/Heatpads.jpg" type="image/jpeg">
  <img src="assets/images/rethinking-spaces-heating/Heatpads.jpg" alt="Microwaveable heat pads used in my home">
</picture>_Microwaveable heat pads that are used in my home. The one on the left is a gel filled pad, while the one on the right is filled with rice._

In the past, I have used a space heater to remain comfortable. I would estimate that when I use a heater, it is on half the time the during the period it is used. Using the hour and a half figure from earlier, this equates to three hours of space heating provided comfort per day throughout the heating season. When I use a heat pad, the worst case scenario is to heat the heat pad for four minutes in every 30 minutes, in a microwave that uses 1800 W. In those same three hours of comfort, the heat pad is heated for 24 minutes, which is approximately 0.72 kWh, only a third of the energy used in the space heater case! This is a huge savings, though one important consideration is that in the transition from space heater to heat pad usage, I have improved my use of insulating clothes and blankets which reduces my reliance on any sort of supplementary heating.

So this winter season, if you would like to save some energy (and some money!), consider turning down your thermostat, and using an efficient heating system like a radiant heater, an electric heat pad or blanket, or a microwave heat pad to make up the difference.

## References and Footnotes

[^1]: [United States Energy Information Administration](https://www.eia.gov/energyexplained/use-of-energy/homes.php)
[^2]: [Wikipedia: Thermal Conduction - Fourier's Law](https://en.wikipedia.org/wiki/Thermal_conduction#Fourier's_law)
[^3]: [Wikipedia: R-value (insulation)](https://en.wikipedia.org/wiki/R-value_(insulation))
[^4]: [NCEI CDO](https://www.ncdc.noaa.gov/cdo-web/datasets)
[^5]: [Availabe on Gitlab](https://gitlab.com/fragile-little-world/fragile-little-world/snippets/1936149)
[^6]: [Chicago Heat Ordinance](https://www.chicago.gov/city/en/depts/bldgs/supp_info/chicago-heat-ordinance.html)
[^7]: [United States Energy Information Administration](https://www.eia.gov/totalenergy/data/browser/index.php?tbl=TA6#/?f=A&start=1949&end=2019&charted=5-6-7-8)
[^8]: [Natural Gas Furnaces](https://naturalgasefficiency.org/for-residential-customers/heat-gas_furnace/)
[^9]: [Energy.gov: Electric Blanket Delivers K.O. to Space Heater During #EnergyFaceoff Round Three](https://www.energy.gov/energysaver/articles/electric-blanket-delivers-ko-space-heater-during-energyfaceoff-round-three)
[^10]: [Wikipedia: Hot water bottle](https://en.wikipedia.org/wiki/Hot_water_bottle)
