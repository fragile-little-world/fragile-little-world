---
title: Contact
---

# How to Get in Touch

## Email

If you have comments, concerns, questions, or just want to chat, please email at [contact@fragilelittle.world](mailto:contact@fragilelittle.world).
