# About Fragile Little World

## Who:

Online you'll find me as Gyrfalcon or Gyrfalcon05.

## What:

Fragile Little World is a website about a variety of topics, but chiefly it focuses on space exploration and sustainability. In keeping with the emphasis on sustainability, no page of the site will require more than [1 MB of bandwidth.](https://www.fragilelittle.world/2019/11/25/why-this-website-is-one-megabyte-per-page.html)

## Where:

Right here on [fragilelittle.world](https://fragilelittle.world), with code available [on Gitlab](https://gitlab.com/fragile-little-world/fragile-little-world).

## When:

New posts should be up approximately once a month.

## Why:

Mostly because I want to, and partly because I wanted to have a project for [Tildes](https://tildes.net/) [Timasomo 2019](https://tildes.net/~creative/iq4/timasomo_post_0_roll_call_and_planning). Also because I have worked on a number of little projects over time, but rarely created anything resembling a deliverable from them. Having a site like this, even if no one else reads it, gives me somewhere to put a final product and some shape to what that product should be.

The name comes from what I find motivates my interest in both outer space and in preserving the Earth. It is also from the words of Neil deGrasse Tyson in describing the Earth. At the end of the day, I find these topics interesting because of their ability to address the fragility of our world, whether through ameliorating climate change or giving humanity a chance to restart.


[![Netlify Status](https://api.netlify.com/api/v1/badges/af6e3d03-06a9-4e34-a2eb-ad284ea74871/deploy-status)](https://app.netlify.com/sites/fragilelittleworld/deploys)
