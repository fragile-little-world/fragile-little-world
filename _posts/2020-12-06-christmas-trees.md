---
topic: Green Alternatives
layout: post
title: "Finding the \"Greenest\" Tree this Holiday"
---

<picture>
  <source srcset="/assets/images/christmas-trees/tree-small.webp" type="image/webp">
  <source srcset="/assets/images/christmas-trees/tree-small.jpg" type="image/jpeg">
  <img src="/assets/images/christmas-trees/tree-small.jpg" alt="An ornament on my Christmas tree">
</picture>_An ornament on my Christmas tree at home, a Grand Fir._

Christmas trees are a fixture in homes across the United States during December. While many families choose an artificial or natural tree based on tradition, ease of use, or other factors, few consider which is the more environmentally friendly option. Unfortunately, like all good questions, the answer is that it depends.

<!--more-->

## A Tale of Two Lifecycle Cost Analyses

The chief engineering tool used to answer questions like this one is the __lifecycle cost analysis__ (LCA). These analyses use a combination of standardized datasets, often provided by governmental agencies like the EPA or standards organizations like ISO, along with (hopefully) primary source data to come up with an estimate for the environmental impacts of a single product at each stage of its life. Ths includes familiar impact categories such as nonrenewable energy demand, global warming potential/CO<sub>2</sub> emissions, and smog production, as well as impact categories that get less airtime, like eutrophication potential in waterways due to excess nitrogen. Once these impact categories are known, consumers and manufacturers can use that information to inform their choices based on their own priorities. Of course, the results of a given LCA can vary depending on the assumptions, boundary conditions, and quality of data available to the authors. As we'll see in the two LCAs examined below, these changes can give significantly different results. However, this does not diminish the value of any one LCA, as combining LCAs that consider somewhat different situations can be useful, which we will do today.

### LCA 1: American Christmas Tree Association (ACTA)/WAP Sustainability Consulting

This LCA[^acta] focused on looking at the mode of both Christmas tree markets, considering the most common model of artificial tree against what they deemed the most common supply chain for a natural tree, scaled to the same size of 6.5 feet. In this case, that is an artificial tree made in China and shipped to the U.S. by boat, and a Fraser Fir grown in the Southeastern U.S. Both this LCA and the second one we will discuss will neglect any lighting and decorations on the trees, since it is safe to assume that they would be comparable. It is important to note that the most common artificial tree used as reference in this study is prelit, and my personal observations are that most artificial trees offered in stores are as well.

The findings show that the chief impact of both the artificial and natural trees happens in production/cultivation, with disposal as the second most important category for the natural tree across three end of life scenarios: landfill, composting, and incineration. The artificial tree was assumed to be landfilled, since the mix of PVC, steel, and polyethylene is hard to separate for recycling. Transportation is significant in both cases, although the transportation of the artificial tree from China to the U.S. is surprisingly low impact, a testament to the relative efficiency of maritime shipping.

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
  overflow:hidden;padding:10px 5px;word-break:normal;}
.tg th{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
  font-weight:normal;overflow:hidden;padding:10px 5px;word-break:normal;}
.tg .tg-34fe{background-color:#c0c0c0;border-color:inherit;text-align:center;vertical-align:top}
.tg .tg-lqy6{text-align:right;vertical-align:top}
.tg .tg-0pky{border-color:inherit;text-align:left;vertical-align:top}
.tg .tg-dvpl{border-color:inherit;text-align:right;vertical-align:top}
.tg .tg-1922{background-color:#32cb00;border-color:inherit;text-align:right;vertical-align:top}
.tg .tg-0lax{text-align:left;vertical-align:top}
.tg .tg-bv1d{background-color:#32cb00;text-align:right;vertical-align:top}
</style>
<table class="tg">
<thead>
  <tr>
    <th class="tg-34fe" rowspan="2">Impact Category</th>
    <th class="tg-34fe" rowspan="2"><br>Unit<br></th>
    <th class="tg-34fe" rowspan="2">Artificial Tree</th>
    <th class="tg-34fe" colspan="3">Natural Tree</th>
  </tr>
  <tr>
    <td class="tg-34fe">Composting</td>
    <td class="tg-34fe">Incineration</td>
    <td class="tg-34fe">Landfill</td>
  </tr>
</thead>
<tbody>
  <tr>
    <td class="tg-0pky">Nonrenewable Primary Energy Demand (PED)</td>
    <td class="tg-0pky">MJ</td>
    <td class="tg-dvpl">284</td>
    <td class="tg-dvpl">101.503</td>
    <td class="tg-1922">-97.182</td>
    <td class="tg-dvpl">106.888</td>
  </tr>
  <tr>
    <td class="tg-0pky">Acidification Potential (AP)</td>
    <td class="tg-0pky">kg SO2 Equivalent</td>
    <td class="tg-dvpl">0.088</td>
    <td class="tg-dvpl">0.026</td>
    <td class="tg-dvpl">0.007</td>
    <td class="tg-dvpl">0.076</td>
  </tr>
  <tr>
    <td class="tg-0lax">Eutrophication Potential (EP)<br></td>
    <td class="tg-0lax">kg N Equivalent</td>
    <td class="tg-lqy6">0.006</td>
    <td class="tg-lqy6">0.015</td>
    <td class="tg-lqy6">0.014</td>
    <td class="tg-lqy6">0.041</td>
  </tr>
  <tr>
    <td class="tg-0lax">Global Warming Potential (GWP)</td>
    <td class="tg-0lax">kg CO2 Equivalent</td>
    <td class="tg-lqy6">17.911</td>
    <td class="tg-lqy6">4.875</td>
    <td class="tg-lqy6">7.775</td>
    <td class="tg-bv1d">-0.105</td>
  </tr>
  <tr>
    <td class="tg-0lax">Smog Potential</td>
    <td class="tg-0lax">kg O3 Equivalent</td>
    <td class="tg-lqy6">1.5</td>
    <td class="tg-lqy6">0.332</td>
    <td class="tg-lqy6">0.099</td>
    <td class="tg-lqy6">0.439</td>
  </tr>
</tbody>
</table>_Table showing the relative impacts of a natural Christmas tree, including one tenth of a stand assumed to last 10 years, against an artificial tree. The natural tree will incur its costs annually in longer comparisons. Adapted from the ACTA report.[^acta]_

The table here looks like an easy win for the natural tree. Some of the categories for different disposal methods are even negative! But we cannot draw a hasty conclusion, since most people keep their artificial trees for several years, incurring no additional lifecycle costs, while the natural tree must be purchased again every year. The LCA examines 5 year and 10 year lifespans for the artificial tree, and the authors end up concluding that around the 5 year mark is when the artificial tree breaks even with the natural tree, on average. This does neglect that some categories are negative for the natural tree, meaning that that artificial tree is worse in that impact no matter what.

There are a few aspects of this study that raise some criticism, though overall I believe the data here to be useful. The first is that it was commissioned by the American Christmas Tree Association, an industry group for artificial Christmas tree manufacturers. This does mean that they had access to high quality, primary source data on what it takes to produce an artificial tree. However, it is difficult to completely remove the suspicion of financial bias in the preparation of the report. WAP Sustainability did do a good job with selecting a review board and documenting their interactions with that review board, so hopefully any bias there is slight, if present. Another is that they admit that they do not have great data on the growing of trees. They also neglect any carbon sequestration during natural tree cultivation, and the carbon stored in the stump after the tree is harvested. Though the stump contains significant amounts of carbon, I am not sure that neglecting the stump is a huge issue. In most commercial operations the stump is removed so that the land can be replanted, but it would have been nice to at least see some discussion of that aspect. Lastly, the crediting of products produced in various end of life scenarios is unclear. For example, the natural tree incineration gets credit for averting nonrenewable primary energy demand, but it is unclear if that is accounted for in terms of the carbon impact. This separate accounting does make more sense for the artificial tree, since much of the impact is fixed in the petroleum products used in the raw materials, but it makes the comparison between the two more difficult.

A positive for the data quality of this LCA is that it is quite recent, being prepared in the 2017/2018 time frame. They also note that ACTA had a different consulting firm produce a similar report aroung 2009, and they offer some small comparison with that report, noting that through energy consumption in the manufacturing process of artificial trees has been reduced by 43%, the corresponding greenhouse gas emissions have only declined 4%. They attribute this to the large amount of greenhouse gasses created in the making of the steel and plastic used in the artificial tree, which makes sense based on their other findings.

### LCA 2: Ellipsos

This LCA[^ellipsos] also focused on common trees, though they used a slightly different baseline of 7 ft. trees. Additionally, the boundary conditions are quite different, focusing on natural tree production and general tree usage in and around Montreal, CA. The authors were able to get data directly from natural tree farms in their area, resulting in high quality data, but they had to use less accurate data for the production of the artificial trees. Treatment of carbon sequestration by the trees in their fallen needles and stumps was included, and due to the availability of suitable land near Montreal, transportation distances were shorter for the natural tree. Additionally, though the authors only consider landfill and incineration end of life scenarios, they are much clearer in terms of crediting the incineration case for the fuel that it replaces. The fact that in Montreal the trees are replacing very polluting heavy fuel oil also helps the natural tree. These factors combined to lead the authors to a rather different conclusion than LCA 1.

Overall, LCA 2 finds that an artificial tree is only better than a natural tree in all impact categories at a lifespan of 40 years, with the worst category being consumption of mineral resources. I think that is a bit of an unfair comparison, but even on more reasonable metrics, like primary energy demand and global warming potential, the breakeven point is around 20 years. They also repeatedly note that the average artificial tree is only used for 6 years, well below the breakeven point in their findings, and only a little bit longer than the breakeven point found in LCA 1.

There are a couple of caveats to the conclusions of this study. Much like LCA 1, this LCA was prepared by a consulting firm. However, unlike LCA 1, there is not much information on if they were paid by another organization to prepare the report, or if they decided to do so for internal reasons. This makes it difficult to know if there might be any bias one way or the other. Additionally, the data used to prepare LCA 2 is older, with the study itself being published in 2009. While we know from the retrospective section in LCA 1 that the changes to artificial tree production since that time have not been significant, it is possible that natural tree cultivation has changed significantly in the last decade, potentially enough to change conclusions.

## What tree should you get?

This is where things get a little bit complex, because the two LCAs come to different conclusions. LCA 2[^ellipsos], as well as some sources online[^conversation], make an important point: In the grand scheme, this isn't the choice that matters. The difference between the best and worst options is on the scale of carpooling to work or working from home (something many of us now can, or must, do) instead of driving for a few weeks a year. But "carpool to work and get whatever tree" is an unsatisfying answer, and after all, as Sydney Smith is credited with saying:

>It is the greatest of all mistakes to do nothing because you can only do little. Do what you can.

So, if there's not much difference between the two options, continuing to use whatever option fits into your celebration of the holidays best is fine. There is room for improvement in both options, if some care is taken at various points in the process. Whatever you may already own, whether that be an artificial tree or a stand for a natural tree, try not to replace it for as long as possible. The main lifecycle impact of these items occurs before they reach your home, so keeping them in use for as long as possible is the key to reducing total impact for them. If you are purchasing either of those two items, prioritize durability and/or repairability as much as you can in the purchasing process, to ensure a long lifespan. 

One important note on lifespan for the artificial tree, is that the most common models are pre lit. Anecdotally, Christmas lights are perhaps the least durable product anyone plugs in, with every year requiring the purchase of replacements due to electrical failure. From a professional perspective, they last about 6 or 7 years for good quality light strands[^lights]. This is remarkably close to the average lifespan of an artificial tree noted in LCA 2, and it is very possible that lighting failures on prelit trees bring down this average lifespan. Because of this, it would be wise to purchase unlit artificial trees where possible, to allow the tree and the lighting to be replaced separately when either fails, rather than incurring the production costs associated with both when either one fails.

For natural trees, though there are fewer attributes about the tree and its care that can be used to reduce impact, since its lifespan will always be one year, with the exception of the tree stand as mentioned previously. One thing that can help is to try to find trees grown closer to where you live, since that means the trees have not been shipped as far to reach you, reducing the footprint due to transportation. Unfortunately, this isn't always possible, especially if the local climate is poor for growing Christmas trees. On the disposal end, check to see what options are available to give your tree a second use, whether that means composting, mulching, or incineration[^recycle]. Many municipal waste systems have special collections for Christmas trees to facilitate these extra uses, which not only help to displace more environmentally damaging products like conventional fertilizers, but can also help to sequester the carbon in the tree if it is not incinerated. While landfilling will technically sequester the most carbon, it also contributes to the problem that is landfills, so it is best to avoid that if possible.

Extending the life of artificial trees, and carefully selecting and disposing of natural trees is helpful, but it can only help so much. There are other changes that could be made to the production systems of Christmas trees that could help to reduce their impacts as well, and those changes are worth examining.

## Improving the System

For artificial trees, the biggest systemic improvements come in the manufacturing stage, since this is where much of the overall environmental impact is determined. As mentioned previously, an increased focus on durability would be helpful in reducing the total need for artificial trees, and thus the total impact, though that may not go over well with the manufacturers. The materials used are also important to the impact. The artificial tree examined in both of the LCAs was primarily made of steel and PVC plastic. These materials are costly in terms of energy to produce, especially steel from ore rather than recycled feedstock. While steel is easy to recycle, PVC is not an easily recyclable plastic, and with materials mingled closely together in the trees it is not economical to separate the components for recycling anyway. Replacing the PVC plastic with another, more recyclable plastic could help (though plastic is becoming more difficult to recycle in the U.S.[^plastic]). Making the plastic biodegradable could also be a positive. Combining this with design changes to allow the different materials to be more easily separated, ideally by the consumer before even entering the waste stream, could reduce the end of life impacts, and save energy in the production of new products through the use of recycled materials.

There are a few interesting options for changing the production systems of natural trees. The first is to use a potted tree[^potted], which can be rented annually or purchased for reuse. The tree remains potted during the holiday season, and then is replanted or replaced outside after the holidays, with the exact method depending on how the tree is kept. This does limit the amount of time the tree can remain indoors, as if it is kept inside for too long, it will lose the ability to survive the cold and die when put back outside. This system allows a tree to continue to sequester carbon for several additional years. Once it is too big for the home, it can be used as an outdoor Christmas or landscaping tree, planted permanently as part of a reforestation project, or disposed of in the manner of natural trees discussed previously.

A second possibility is coppicing. __Coppicing__ is an ancient forestry technique whereby a tree is cut in such a way that new saplings will sprout from the stump. In many cases, this is done to produce many sticks of about the same diameter and length, for firewood or manufacturing feedstock, but can be done to regrow entire trees from the same root system if managed properly. This technique eliminates the need for heavy machinery and stump disposal in the process of producing trees, and keeps the carbon in the root system sequestered longer. Additionally, since the root system continues to grow over the life of several trunks, the trees are more resistant to drought and need less added water. This would require more careful management of individual trees, and is somewhat more difficult with the tree cultivars used for Christmas trees than other types of trees, but it is being done[^coppice] in the U.S. with promising results.

A third concept is the use of __relay intercropping__[^relay] to help Christmas tree farms make economic sense closer to population centers, reducing emissions due to transportation of the cut trees. In relay intercropping, crops that take a long time to mature, like fruit trees or Christmas trees, are planted in alternate rows with a faster maturing crop. This can have agricultural or ecological benefits depending on the crops in question, but in the case of Christmas trees, the advantage is primarily one of reducing financial risk. Purchasing a plot of land to plant Christmas trees may not see any return for a decade or longer. This makes it rather risky, and thus it is often conducted on what is called __marginal land__, which is land that is some combination of far from distribution systems, infertile, and difficult to farm, to the point that farming most crops there is not worth the cost of the land. The low cost of using otherwise marginal land helps alleviate the risk of growing Christmas trees, but it also means that even in climates where Christmas trees grow well, they are not grown near where most people live. By using relay intercropping, the risk is spread out over two (or more) crops, and the enterprise can earn revenue every year, allowing the use of more expensive land closer to population centers. Additionally, if located close to population centers, crops like melons and berries can be sold on site at retail prices, increasing revenue potential.

These types of systemic changes will not happen without cause. As consumers, we can look for trees that meet our criteria of environmental impact, and let manufacturers, farmers, and retailers know how they can better meet our needs. As entrepreneurs, we can start businesses that produce in keeping with our values. And most importantly, as citizens we can lobby and vote for politicians who will create policy promoting positive change.

Do what you can.

## References and Footnotes

[^acta]: WAP Sustainability Consulting, LLC., (2018) _Comparative LCA of the Environmental Impacts of Real Christmas and Artificial Christmas trees_. Retrieved from [Archive.org](https://web.archive.org/web/20201112003127/https://8nht63gnxqz2c2hp22a6qjv6-wpengine.netdna-ssl.com/wp-content/uploads/2018/11/ACTA_2018_LCA_Study.pdf)
[^ellipsos]: Couillard S., Bage G., Trudel J., (2009) _Comparative Life Cycle Assessment (LCA) Of Artificial vs Naturla Christmas Tree_. Retrieved from [ellipsos.ca](http://ellipsos.ca/lca-christmas-tree-natural-vs-artificial/)
[^conversation]: Cregg B., (2018) _Don’t stress about what kind of Christmas tree to buy, but reuse artificial trees and compost natural ones_. Retrieved from [theconversation.com](https://theconversation.com/dont-stress-about-what-kind-of-christmas-tree-to-buy-but-reuse-artificial-trees-and-compost-natural-ones-108230)
[^lights]: Woodward J., (2016) _How Long Do LED Christmas Lights Really Last?_. Retrieved from [christmasdesigners.com](https://www.christmasdesigners.com/blog/how-long-do-led-christmas-lights-really-last/)
[^recycle]: _How to recycle: Real Christmas Trees have a second life_. (n.d.) Retrieved from [realchristmastrees.org](https://realchristmastrees.org/All-About-Trees/How-to-Recycle/)
[^plastic]: Whitcomb I., (2020) _How much plastic actually gets recycled?_. Retrieved from [livescience.com](https://www.livescience.com/how-much-plastic-recycling.html)
[^potted]: Schumann N., (2019) _Buying a Potted Christmas Tree This Year? Here's What to Know_. Retrieved from [countryliving.com](https://www.countryliving.com/gardening/g28901123/potted-christmas-tree/)
[^coppice]: Katsnelson A., (2018) _Stump-Grown Christmas Trees Are the Gift That Keeps on Giving_. Retrieved from [smithsonianmag.com](https://www.smithsonianmag.com/science-nature/coppice-farming-grows-christmas-trees-keep-giving-180971068/)
[^relay]: Lamont W.J., Hensley D.L., Wiest S., Gaussoin R.E., (1993) _Relay-Intercropping Muskmelons with Scotch Pine Christmas Trees Using Plastic Mulch and Drip Irrigation_. Retrieved from [digitalcommons.unl.edu](https://digitalcommons.unl.edu/agronomyfacpub/646/)